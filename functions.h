/*
 * functions.h
 *
 *  Created on: Oct 26, 2014
 *      Author: Batterypowered7
 */

#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <string>
#include <cctype>
using namespace std;

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

string getInfixExpression(const string&); //Converts Postfix to Infix notation
void radixSort(vector<string>&, const int&, const int&); //Implements radix sort
bool isOperator(const char&); //Helper function to find operators within strings

/******************************************************************************
* Converts a string containing an expression written in Reverse Polish
* Notation (Postfix notation) into the same expression written in Infix
* notation.
*
* Precondition: A string that contains a mathematical expression written in
* Reverse Polish Notation (Postfix notation).
*
* Postcondition: Returns a string containing the expression contained within
* the original string in Infix notation.
******************************************************************************/
string getInfixExpression(const string& postfixExpression)
{

   //Declare a string to hold the infix expression to be pushed onto the stack
   string infixExpression;
   stack <string> functionStack;

   //Read the input string left-to-right.
   for(int i = 0; i < postfixExpression.length(); ++i)
   {

      //When you encounter an operator, use the top two items from the stack
      //to make an operand.
      if(isOperator(postfixExpression[i]))
      {
         //Create a temporary string to hold the character at location i
         string temp;

         //We'll be inserting things at the beginning of the expression so
         //the closing parenthesis comes first.
         infixExpression.insert(0, ")");

         //Insert the second operand, then remove it from the stack.
         infixExpression.insert(0, functionStack.top());
         functionStack.pop();

         //Store the operator in the temp variable, then insert it into
         //the expression.
         temp += postfixExpression[i];
         infixExpression.insert(0, temp);

         //Insert the first operand, then remove it from the stack.
         infixExpression.insert(0, functionStack.top());
         functionStack.pop();

         //Insert the opening parenthesis, then push the expression onto
         //the stack.
         infixExpression.insert(0, "(");
         functionStack.push(infixExpression);

         //Clear the expression so we may start fresh.
         infixExpression.clear();
      }

      //Put operands in the stack.
      else
      {
         string temp;
         temp += postfixExpression[i];
         functionStack.push(temp);
      }

   }

   //After the string has been read in then the only item on the stack will be
   //the infix expression.
   return functionStack.top();
}


/******************************************************************************
* Uses the radix sort algorithm to sort numbers stored as strings within a
* vector.
*
* Precondition: A vector of strings containing the numbers to be sorted,
* the maximum length of any number in the vector, and the radix (number base)
* of the numbers to be sorted.
*
* Postcondition: The strings within the vector should be sorted in ascending
* order.
******************************************************************************/
void radixSort(vector<string>& numbers, const int& digitsPerNumber, const int& radix)
{
   //Create a queue for each possible number that can be in a number.
   vector <queue <string> > buckets;
   for (int i = 0; i < radix; ++i)
   {
      queue <string> bucket;
      buckets.push_back(bucket);
   }

   //Iterate over each number's digits, starting with the least significant
   for (int i = digitsPerNumber - 1; i >= 0; --i)
   {
      //Declare an iterator that points to the first string
      vector<string>::iterator it = numbers.begin();

      //Iterate over each the numbers in the vector and put them in a bucket
      for(; it != numbers.end(); ++it)
      {
         //Declare an iterator that points to the relevant digit
         string::iterator it2 = it->begin()+i;
         int digitI;

         //Capital letters in ASCII are valued 65 through 90. Check to see if
         //the digit is a letter, and subtract 55 to arrive the correct value.
         if(isalpha(*it2))
         {
            digitI = (*it2 - 55);
         }

         //Number characters in ASCII are valued 48 through 57. Subtract 48
         //from these to arrive at the correct value.
         else
         {
            digitI = *it2 - 48;
         }

         //Push the string into the correct bucket (queue).
         buckets[digitI].push(*it);
      }

      //Clear the vector of all strings previously stored in it.
      numbers.clear();

      //Iterate through the buckets, adding the values sorted at each step
      //to the vector, then removing them from each bucket.
      for (int i = 0; i < radix; ++i)
      {
         while(!buckets[i].empty())
         {
            numbers.push_back(buckets[i].front());
            buckets[i].pop();
         }
      }
   }
}


/******************************************************************************
* Determines whether a character is an operator by running it through a switch
* statement. The following symbols are considered operators: +, -, *, and /
*
* Precondition: A character variable
*
* Postcondition: Returns true if the character variable is an operator and
* false otherwise.
******************************************************************************/
bool isOperator(const char& character)
{
	switch(character)
	{
		case '+':

			return true;
			break;

		case '-':

			return true;
			break;

		case '*':

			return true;
			break;

		case '/':

			return true;
			break;

		default:

			return false;
	}
}
#endif /* FUNCTIONS_H_ */
