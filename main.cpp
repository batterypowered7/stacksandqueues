/*
 * main.cpp
 *
 *  Created on: Oct 26, 2014
 *      Author: Batterypowered7
 */

#include "functions.h"
#define DECIMAL 10
#define HEXADECIMAL 16
#define TRIGESIMAL 30

int main()
{

/******************************************************************************
*
* 		THIS SECTION IS USED TO TEST THE getInfixExpression FUNCTION
*
******************************************************************************/

   //Declare three strings with in correct postfix notation.
   string testString("ab+");
   string testString2("ab+cd-*");
   string testString3("ab+cd-*ef/*");

   //Print the contents of testString and the results of running the function
   cout << "testString in postfix notation: " << testString << endl;
   cout << "testString as infix: " << getInfixExpression(testString) << endl;
   cout << endl;

   //Print the contents of testString2 and the results of running the function
   cout << "testString2 in postfix notation: " << testString2 << endl;
   cout << "testString2 as infix: " << getInfixExpression(testString2) << endl;
   cout << endl;

   //Print the contents of testString3 and the results of running the function
   cout << "testString3 in postfix notation: " << testString3 << endl;
   cout << "testString3 as infix: " << getInfixExpression(testString3) << endl;
   cout << endl;

/******************************************************************************
*
* 			  THIS SECTION IS USED TO TEST THE radixSort FUNCTION
*
******************************************************************************/

   //Create an array of strings to initialize vector<string> numbers with.
   string Values[] = {"234", "123", "234", "678", "456", "567"};
   vector<string> numbers(Values,Values+sizeof(Values)/sizeof(Values[0]));

   //Display the contents of numbers before sorting.
   cout << "vector<string> numbers before sorting:" << endl;
   for(int i = 0; i < numbers.size(); ++i)
   {
      cout << numbers[i] << endl;
   }
   cout << endl;

   //Run the radixSort function on numbers.
   radixSort(numbers, numbers[0].length(), DECIMAL);

   //Display the contents of numbers after sorting.
   cout << "vector<string> numbers after sorting:" << endl;
   for(int i = 0; i < numbers.size(); ++i)
   {
      cout << numbers[i] << endl;
   }
   cout << endl;

   //Create an array of strings to initialize vector<string> numbers2 with
   string Values2[] = {"FFFF", "B00B", "8008", "4E11", "0000"};
   vector<string> numbers2(Values2,Values2+sizeof(Values2)/sizeof(Values2[0]));

   //Display the contents of numbers2 before sorting.
   cout << "vector<string> numbers2 before sorting:" << endl;
   for(int i = 0; i < numbers2.size(); ++i)
   {
      cout << numbers2[i] << endl;
   }
   cout << endl;

   //Run the radixSort function on numbers2.
   radixSort(numbers2, numbers2[0].length(), HEXADECIMAL);

   //Display the contents of numbers2 after sorting.
   cout << "vector<string> numbers2 after sorting:" << endl;
   for(int i = 0; i < numbers2.size(); ++i)
   {
      cout << numbers2[i] << endl;
   }
   cout << endl;

   //Create an array of strings to initialize vector<string> numbers3 with
   string Values3[] = {"T4ST3", "HASTE", "12345", "ELITE"};
   vector<string> numbers3(Values3,Values3+sizeof(Values3)/sizeof(Values3[0]));

   //Display the contents of numbers3 before sorting.
   cout << "vector<string> numbers3 before sorting:" << endl;
   for(int i = 0; i < numbers3.size(); ++i)
   {
      cout << numbers3[i] << endl;
   }
   cout << endl;

   //Run the radixSort function on numbers3.
   radixSort(numbers3, numbers3[0].length(), TRIGESIMAL);

   //Display the contents of numbers3 after sorting.
   cout << "vector<string> numbers3 after sorting:" << endl;
   for(int i = 0; i < numbers3.size(); ++i)
   {
      cout << numbers3[i] << endl;
   }
   cout << endl;

}
